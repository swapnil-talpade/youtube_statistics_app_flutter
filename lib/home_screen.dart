import 'package:flutter/material.dart';
import 'components/rounded_button.dart';
import 'model/channel_model.dart';
import 'services/api_service.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _controller = TextEditingController();
  bool isLoading = false;
  String channel_id = "";
  Channel _channel;

  @override
  void initState() {
    super.initState();
    _initChannel();
  }

  _initChannel() async {
    Channel channel = await APIService.instance
        .fetchChannel(channelId: 'UCBJycsmduvYEL83R_U4JriQ');
    setState(() {
      _channel = channel;
    });
  }

  getChannel() async {
    Channel channel =
        await APIService.instance.fetchChannel(channelId: channel_id);
    setState(() {
      _channel = channel;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SubsCount App"),
        backgroundColor: Colors.red,
      ),
      body: _channel != null
          ? SingleChildScrollView(
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      TextField(
                        controller: _controller,
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            borderSide: BorderSide(width: 1, color: Colors.red),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            borderSide: BorderSide(width: 1, color: Colors.red),
                          ),
                          hintText: "Enter Channel Id",
                        ),
                      ),
                      RoundedButton(
                        title: "Get Data",
                        colour: Colors.red,
                        onPressed: () {
                          setState(() {
                            channel_id = _controller.text;
                            getChannel();
                          });
                        },
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Container(
                        margin: EdgeInsets.all(10.0),
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                              offset: Offset(0, 1),
                              blurRadius: 6.0,
                            ),
                          ],
                        ),
                        child: Row(
                          children: <Widget>[
                            CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 35.0,
                              backgroundImage:
                                  NetworkImage(_channel.profilePictureUrl),
                            ),
                            SizedBox(width: 12.0),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    _channel.title,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  Text(
                                    'Subscribers: ${_channel.subscriberCount}',
                                    style: TextStyle(
                                      color: Colors.grey[600],
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  Text(
                                    'Videos: ${_channel.videoCount}',
                                    style: TextStyle(
                                      color: Colors.grey[600],
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  Text(
                                    'Total Views: ${_channel.viewCount}',
                                    style: TextStyle(
                                      color: Colors.grey[600],
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            Text("Some Well known Channels"),
                            Row(
                              children: <Widget>[
                                RoundedButton(
                                  title: "PieDiePie",
                                  colour: Colors.red,
                                  onPressed: () {
                                    setState(() {
                                      channel_id = "UC-lHJZR3Gqxm24_Vd_AJ5Yw";
                                      getChannel();
                                    });
                                  },
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                RoundedButton(
                                  title: "T-Series",
                                  colour: Colors.red,
                                  onPressed: () {
                                    setState(() {
                                      channel_id = "UCq-Fj5jknLsUf-MWSy4_brA";
                                      getChannel();
                                    });
                                  },
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                RoundedButton(
                                  title: "SaimanSays",
                                  colour: Colors.red,
                                  onPressed: () {
                                    setState(() {
                                      channel_id = "UCy9cb7U-Asbhbum0ZXArvfQ";
                                      getChannel();
                                    });
                                  },
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                RoundedButton(
                                  title: "AIB",
                                  colour: Colors.red,
                                  onPressed: () {
                                    setState(() {
                                      channel_id = "UCzUYuC_9XdUUdrnyLii8WYg";
                                      getChannel();
                                    });
                                  },
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                RoundedButton(
                                  title: "BB ki Vines",
                                  colour: Colors.red,
                                  onPressed: () {
                                    setState(() {
                                      channel_id = "UCqwUrj10mAEsqezcItqvwEw";
                                      getChannel();
                                    });
                                  },
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                RoundedButton(
                                  title: "CarryMinati",
                                  colour: Colors.red,
                                  onPressed: () {
                                    setState(() {
                                      channel_id = "UCj22tfcQrWG7EMEKS0qLeEg";
                                      getChannel();
                                    });
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          : Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  Theme.of(context).primaryColor, // Red
                ),
              ),
            ),
    );
  }
}
